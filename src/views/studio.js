import React,{Component} from "react";
import Container from "../../node_modules/react-bootstrap/Container";
import StudioS1 from "../components/studio-s1";
import StudioS2 from "../components/studio-s2";
import StudioS3 from "../components/studio-s3";

class Studio extends Component{
    render(){
        return(        
            <Container fluid className="minh-footer-adj p-0">
                <StudioS1/>
                <StudioS2/>
                <StudioS3/>
            </Container>    
        );
    }
}

export default Studio;