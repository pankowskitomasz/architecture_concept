import React,{Component} from "react";
import Container from "../../node_modules/react-bootstrap/Container";
import ProjectsS1 from "../components/projects-s1";
import ProjectsS2 from "../components/projects-s2";
import ProjectsS3 from "../components/projects-s3";

class Projects extends Component{
    render(){
        return(        
            <Container fluid className="minh-footer-adj p-0">
                <ProjectsS1/>
                <ProjectsS2/>
                <ProjectsS3/>
            </Container>    
        );
    }
}

export default Projects;